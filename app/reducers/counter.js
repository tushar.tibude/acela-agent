// @flow
import { INCREMENT_COUNTER, DECREMENT_COUNTER, FETCH_DATA_REQUEST,FETCH_DATA_SUCCESS,FETCH_DATA_FAILURE } from '../actions/counter';
import type { Action } from './types';

export default function counter(state: number = 0, action: Action) {
  switch (action.type) {
    case INCREMENT_COUNTER:
      return state + 1;
    case DECREMENT_COUNTER:
      return state - 1;

    case FETCH_DATA_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_DATA_SUCCESS:
      return Object.assign({}, state, {
        data: action.response,
        isFetching: false,
      });

    case FETCH_DATA_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });
    default:
      return state;
  }
}
