// @flow
import { FETCH_AUTH_REQUEST,FETCH_AUTH_SUCCESS,FETCH_AUTH_FAILURE } from '../actions/authentication';
import type { Action } from './types';

const initialState: IAppState = {
  isFetching: false,
  data: null,
}
export default function authentication(state: IAppState = initialState, action: Action) {
  switch (action.type) {
    case FETCH_AUTH_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_AUTH_SUCCESS:
      return Object.assign({}, state, {
        //isVa: action.response,
        isFetching: false,
      });

    case FETCH_AUTH_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });
    default:
      return state;
  }
}
