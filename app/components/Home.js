// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import routes from '../constants/routes.json';
import styles from './Home.css';
import { Formik } from 'formik';
import { validateToken } from './../../backend/api'
type Props = {};
type State = {};
import SolarwindsService from './../../backend/solarwindsService'
import localStorageJSON from './../../backend/db';
import { withRouter } from 'react-router';
let storage = new localStorageJSON();
let solar = new SolarwindsService();
class Home extends Component<Props, State> {
  props: Props;

  // componentDidMount(){
  //       const token = storage.tokenExists();
  //         if (token) {
  //            this.props.history.push('/counter')
  //         }
  // }

  onCLickCounter =() =>{
    this.props.history.push('/counter')
  }
  render() {
    return (
      <div className={styles.container} data-tid="container">
        <h2>Home</h2>
       <Formik
      initialValues={{ token: '' }}
      validate={values => {
        const errors = {}
        if (!values.token) {
          errors.token = 'Token required';
        }
        return errors;
      }}
      onSubmit={(values,{setErrors}) => {
        const isValid = validateToken(values.token);
        if (isValid) {
          console.info("Valid!!!");
          storage.setToken(values.token);
          const token = storage.tokenExists();
          if (token) {
             this.props.history.push('/counter')
          }
        }
        else{
          setErrors({
            token: 'Invalid token',
          })
        }


      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        /* and other goodies */
      }) => (
        <form onSubmit={handleSubmit} className={styles.credForm}>
          <div className={styles.fieldBox}>
            <label className={styles.fieldLabel}>Agent Token</label>
            <div  className={styles.field}>
              <input
                type="token"
                name="token"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.token}
                placeholder="Agent Token"
                className={styles.inputBox}
              />
              <div  className='error-message'>{errors.token && touched.token && errors.token}</div>
            </div>
          </div>

          <button className={`${'primary'}  ${styles.submit}`} type="submit">
            Validate
          </button>
        </form>
      )}
    </Formik>

    <Link to="/test">test</Link>----
    <Link to="/counter">counter</Link>---
    <Link to="/integrations">integrations</Link>---

    <button className={`${'primary'}`} onClick={this.onCLickCounter} type="button">
    test
          </button>
      </div>
    );
  }
}
export default withRouter(Home);
