// @flow
import React, { Component } from 'react';
import { Formik } from 'formik';
import styles from './integrations.css';
import SolarwindsService from '../../../backend/solarwindsService';
import Spinner from '../../sharedcomponents/Spinner/index';
import { Link } from 'react-router-dom';
import routes from '../../constants/routes';
import { getDateTimeWithFormat } from '../../utils/DateUtils';

const solarwinds = new SolarwindsService();
type Props = {
  getDataAuth: any
};

class Integrations extends Component<Props> {
  props: Props;

  // eslint-disable-next-line flowtype/no-weak-types
  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  componentDidMount() {
    this.getSetSolawindCredentials();
    const isAuthenticationCompleted = solarwinds.isAuthenticationCompleted();
    if (isAuthenticationCompleted) {
      this.setState({ isAuthenticationCompleted });
    }
    const isIntegrationCompleted = solarwinds.isIntegrationCompleted();
    if (isIntegrationCompleted) {
      this.setState({ isIntegrationCompleted });
    }
    setInterval(
      () => {
        this.checkIntegrationCompleted();
        this.setState({ state: this.state })
      },
      20000
    );
  }
  getSetSolawindCredentials =()=>{
    const credentials = solarwinds.getCredentials();
    if (credentials) {
      this.setState({
        host: credentials.server,
        port: credentials.port,
        username: credentials.auth.username,
        password: credentials.auth.password
      });
    }
  }
  getEmptyState = () => ({
    validationMessage: '',
    fetching: false,
    edit: false,
    protocol: 'https://',
    host: '',
    port: 17778,
    username: '',
    password: '',
    isAuthenticationCompleted: false,
    isIntegrationCompleted: false
  });

  checkIntegrationCompleted = () => {
    if (!this.state.isIntegrationCompleted) {
      const isIntegrationCompleted = solarwinds.isIntegrationCompleted();
      if (isIntegrationCompleted) {
        this.setState({ isIntegrationCompleted });
      }
    }
  }
  onCLickIntegrations = () => {
    this.props.getDataAuth();
  };

  getLastRunTimestampSolarwinds = () => {
    const utcSeconds = solarwinds.getLastRunTimestamp();
    return getDateTimeWithFormat(utcSeconds);
  }

  getDevicesSyncedCount = () => {
    const count = solarwinds.getDeviceCount();
    return count ? count : 'N.A.';
  }

  onCLickEdit = () => {
    this.getSetSolawindCredentials();
    this.setState({
      isAuthenticationCompleted: false,
      isIntegrationCompleted: false,
      validationMessage: '',
      edit: true
    });
  };

  onCLickCancel = () => {
    if (this.state.edit) {
      this.setState({
        isAuthenticationCompleted: true,
        edit: false
      });
    } else {
      this.props.history.push(routes.SERVICES);
    }
  };

  render() {
    return (
      <div className="main-container">
        {/* <div className={styles.backButton} data-tid="backButton">
          <Link to={routes.AUTHENTICATION}>
            <i className="fa fa-arrow-left fa-3x" />
          </Link>
        </div> */}
        <div className={styles.container} data-tid="container">
          <div className={styles.solarwindHeading}>
            Solarwinds API Integration
          </div>

          <div className="back-button" title="back to services page">
            <Link to={routes.SERVICES}> Home</Link>
          </div>
          {(this.state.isAuthenticationCompleted === false ||
            this.state.edit) && (
              <Formik
                enableReinitialize
                initialValues={{
                  protocol: 'https://',
                  host: this.state.host,
                  port: 17778,
                  username: this.state.username,
                  password: this.state.password
                }}
                validate={values => {
                  const errors = {};
                  if (!values.protocol) {
                    errors.protocol = 'Protocol required';
                  }
                  if (!values.host) {
                    errors.host = 'Host/IP required';
                  }
                  if (!values.port) {
                    errors.port = 'Port required';
                  }
                  if (!values.username) {
                    errors.username = 'Username required';
                  }
                  if (!values.password) {
                    errors.password = 'Password required';
                  }
                  return errors;
                }}
                onSubmit={(values, { setErrors }) => {
                  this.setState({ fetching: true });
                  solarwinds
                    .validate(
                      values.host,
                      values.port,
                      values.username,
                      values.password
                    )
                    .then(result => {
                      if (result.isValid) {
                        solarwinds.addServiceIntegration()
                        solarwinds.saveCredentials(
                                values.host,
                                values.port,
                                values.username,
                                values.password
                              );
                              this.setState({
                                isAuthenticationCompleted: result.isValid
                              });
                      }
                      this.setState({
                        validationMessage: result.message,
                        fetching: false
                      });
                    });
                }}
              >
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit
                  /* and other goodies */
                }) => (
                    <form onSubmit={handleSubmit}>
                      <div className={styles.fieldBox}>
                        <label className={styles.fieldLabel}>Protocol</label>
                        <div className={styles.fiel}>
                          <input
                            type="text"
                            name="protocol"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.protocol}
                            placeholder="Enter Protocol"
                            className={styles.inputBox}
                            disabled
                          />
                          <div className={styles.errorMsg}>
                            {errors.protocol && touched.protocol && errors.protocol}
                          </div>
                        </div>
                      </div>
                      <div className={styles.fieldBox}>
                        <label className={styles.fieldLabel}>Host/IP</label>
                        <div className={styles.field}>
                          <input
                            type="text"
                            name="host"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.host}
                            placeholder="Enter Host/IP"
                            className={styles.inputBox}
                          />
                          <div className="error-message">
                            {errors.host && touched.host && errors.host}
                          </div>
                        </div>
                      </div>
                      <div className={styles.fieldBox}>
                        <label className={styles.fieldLabel}>Port</label>
                        <div className={styles.field}>
                          <input
                            type="text"
                            name="port"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.port}
                            placeholder="Enter Port"
                            className={styles.inputBox}
                            disabled
                          />
                          <div className={styles.errorMsg}>
                            {errors.port && touched.port && errors.port}
                          </div>
                        </div>
                      </div>
                      <div className={styles.fieldBox}>
                        <label className={styles.fieldLabel}>Username</label>
                        <div className={styles.field}>
                          <input
                            type="text"
                            name="username"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.username}
                            placeholder="Enter Username"
                            className={styles.inputBox}
                          />
                          <div className="error-message">
                            {errors.username && touched.username && errors.username}
                          </div>
                        </div>
                      </div>
                      <div className={styles.fieldBox}>
                        <label className={styles.fieldLabel}>Password</label>
                        <div className={styles.field}>
                          <input
                            type="password"
                            name="password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.password}
                            placeholder="Enter Password"
                            className={styles.inputBox}
                          />
                          <div className="error-message">
                            {errors.password && touched.password && errors.password}
                          </div>
                        </div>
                      </div>

                      {this.state.validationMessage && (
                        <div className="error-message message-padding">
                          {this.state.validationMessage}
                        </div>
                      )}
                      <button className={`primary ${styles.submit}`} type="submit">
                        {this.state.edit ? 'Update' : 'Save'}
                      </button>
                      <button
                        className={`${styles.submit}`}
                        onClick={e => this.onCLickCancel()}
                        type="button"
                      >
                        Cancel
                  </button>
                    </form>
                  )}
              </Formik>
            )}

          {this.state.isAuthenticationCompleted === true &&
            this.state.isIntegrationCompleted === false &&
             this.state.edit === false && (
              <div className={styles.SolarwindsCredentialsVerifiedBOX}>
                <ul className={styles.listingUL}>
                  <li className={styles.listingLI}>
                    <span
                      className={styles.listingULSpan}
                      style={{ color: '#3BB54A' }}
                    >
                      <i className="fas fa-check-circle"></i>
                    </span>
                    Credentials Verified
                  </li>
                  <li className={styles.listingLI}>
                    <span
                      className={styles.listingULSpan}
                      style={{ color: '#E3E9ED' }}
                    >
                      <i className="fas fa-check-circle"></i>
                    </span>
                    Acela configuration
                    <p className={styles.SolarwindsCredentialsVerifiedMessage}>
                      Please add configuration on the Acela portal to complete
                      the integration.
                    </p>
                  </li>
                </ul>

                <button
                  className={`${'primary'}`}
                  onClick={e => this.onCLickEdit()}
                  type="button"
                >
                  Edit credentials
                </button>
              </div>
            )}
          {this.state.isAuthenticationCompleted === true &&
            this.state.isIntegrationCompleted === true &&
              this.state.edit === false &&(
              <div className={styles.SolarwindsCredentialsVerifiedBOX}>
                <ul className={styles.listingUL}>
                  <li className={styles.listingLI}>
                    <span
                      className={styles.listingULSpan}
                      style={{ color: '#3BB54A' }}
                    >
                      <i className="fas fa-check-circle"></i>
                    </span>
                    Credentials Verified
                  </li>
                  <li className={styles.listingLI}>
                    <span
                      className={styles.listingULSpan}
                      style={{ color: '#3BB54A' }}
                    >
                      <i className="fas fa-check-circle"></i>
                    </span>
                    Acela configuration
                  </li>
                </ul>
                <div className={styles.SolarwindsBody}>
                  <div className={styles.SolarwindsBox}>
                    <div className={styles.SolarwindsRow}>
                      Last data synced on : <span>{this.getLastRunTimestampSolarwinds()}</span>
                    </div>
                  </div>
                </div>
                <div className={styles.SolarwindsBody}>
                  <div className={styles.SolarwindsBox}>
                    <div className={styles.SolarwindsRow}>
                      Device synced: <span>{this.getDevicesSyncedCount()}</span>
                    </div>
                  </div>
                </div>
                <button
                  className={`${'primary'}`}
                  onClick={e => this.onCLickEdit()}
                  type="button"
                >
                  Edit credentials
                </button>
              </div>
            )}
        </div>
        <Spinner show={this.state.fetching} />
      </div>
    );
  }
}

export default Integrations;
