// @flow
import { CALL_API } from '../middleware/ApiMiddleware';

export const IS_FETCHING = 'IS_FETCHING';
export const FETCHING_COMPLETE = 'FETCHING_COMPLETE';
export const ERROR = 'ERROR';

export const FETCH_AUTH_REQUEST = 'FETCH_AUTH_REQUEST';
export const FETCH_AUTH_SUCCESS = 'FETCH_AUTH_SUCCESS';
export const FETCH_AUTH_FAILURE = 'FETCH_AUTH_FAILURE';

export const validateCollectorToken = (token: string) => ({
  [CALL_API]: {
    endpoint: `/collector-tokens`,
    method: 'post',
    body: { token },
    authenticated: false,
    types: [FETCH_AUTH_REQUEST, FETCH_AUTH_SUCCESS, FETCH_AUTH_FAILURE]
  }
});

export const sendCollectorHeartBeat = payload => ({
  [CALL_API]: {
    endpoint: `/collector-heartbeat`,
    method: 'post',
    body: payload,
    authenticated: false,
    types: [FETCH_AUTH_REQUEST, FETCH_AUTH_SUCCESS, FETCH_AUTH_FAILURE]
  }
});
