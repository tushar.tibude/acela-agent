const { Service } = require('node-windows');

// Create a new service object
const svc = new Service({
  name: 'Acela Collector Service',
  description: 'The nodejs.org example web server.',
  // eslint-disable-next-line global-require
  script: require('path').join(__dirname, './service.js'),
  nodeOptions: ['--harmony', '--max_old_space_size=4096', '--stopparentfirst=y']
});

/* -------------------- Service Install event Handler  -----------*/
// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install', function() {
  svc.start();
});

/* -------------------- Service already Installed event Handler  -----------*/
// Just in case this file is run twice.
svc.on('alreadyinstalled', function() {
  console.log('This service is already installed.');
});

// Listen for the "start" event and let us know when the
// process has actually started working.
svc.on('start', function() {
  console.log(`${svc.name} started!`);
});

// Listen for the "uninstall" event so we know when it's done.
svc.on('uninstall', function() {
  console.log('Uninstall complete.');
  console.log('The service exists: ', svc.exists);
});

export default svc;
// Install the script as a service.
svc.install();

// Uninstall
// svc.uninstall();
// var service = require ("os-service");
// import ping_my_server from './script.js';
// service.add ("acela-service", ping_my_server);
