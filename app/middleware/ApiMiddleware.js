import axios from 'axios';
import { ERROR, FETCHING_COMPLETE, IS_FETCHING } from '../actions/counter';
import { getIdToken } from '../utils/authUtils';

let API_BASE_URL;

// TODO
// This can be removed
export const setAPIBaseUrl = (url: string) => {
  API_BASE_URL = url;
};

export const CALL_API = Symbol('Call API');

export default store => next => action => {
  const callAPI = action[CALL_API];

  // So the middleware doesn't get applied to every single action
  if (typeof callAPI === 'undefined') {
    return next(action);
  }

  const {
    endpoint,
    method,
    params,
    body,
    headers,
    types,
    authenticated,
  } = callAPI;

  // TODO
  // Use the ES6 format while implementing retries.
  // const [requestType, successType, errorType] = types;
  const requestType = types[0];
  const successType = types[1];
  const errorType = types[2];

  store.dispatch({ type: requestType });
  store.dispatch({ type: IS_FETCHING });

  /* Passing the authenticated boolean back in our data will let us
   * distinguish between normal and secret quotes
   */
  return callApi(endpoint, method, params, body, headers, authenticated).then(
    response => {
      store.dispatch({ type: FETCHING_COMPLETE });

      return next({
        type: successType,
        response,
        authenticated,
      });
    },
    error => {
      store.dispatch({
        type: ERROR,
        errorMessage: error.message || 'There was an error.',
        errorDetails: error.response,
      });

      return next({
        type: errorType,
        error: error.message || 'There was an error.',
        errorList: error.response,
      });
    }
  );
};

export function callApi(
  endpoint: string,
  method: string,
  params: any,
  body: any,
  headers: any,
  authenticated: boolean
) {
  const token = getIdToken() || null;
  const mandatoryHeaders = {
    'Content-Type': 'application/json',
  };

  const requestHeaders = Object.assign({}, mandatoryHeaders, headers);
  const config = {
    method,
    params,
    headers: requestHeaders,
    data: body,
    url: API_BASE_URL + endpoint,
  };

  if (authenticated) {
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    } else {
      throw new Error('No token saved!');
    }
  }

  return axios
    .request(config)
    .then(response => response)
    .catch(error => {
      throw error;
    });
}
