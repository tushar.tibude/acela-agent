const HeartBeatService = require('./../backend/heartBeatService');
const HostInfoService = require('./../backend/hostInfoService');
const SolarwindsService = require('./../backend/solarwindsService');

const heartBeatService = new HeartBeatService();
const hostInfoService = new HostInfoService();
const solarwindsService = new SolarwindsService();

const MAIN_SERVICE_POOLING_INTERVAL_IN_SEC = 30;

function mainService() {
  heartBeatService.poll();
  solarwindsService.poll();
  hostInfoService.poll();
  setTimeout(mainService, MAIN_SERVICE_POOLING_INTERVAL_IN_SEC * 1000);
}

mainService();
