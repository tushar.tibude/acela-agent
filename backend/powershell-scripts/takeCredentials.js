const { spawn } = require('child_process');

let child;
const scriptPath = require('path').join(
  process.cwd(),
  './validateLocalAccount.ps1'
);

// eslint-disable-next-line import/prefer-default-export
function inputCredentials() {
  // eslint-disable-next-line prefer-const
  let output = [];
  spawn('powershell.exe', [
    'Set-ExecutionPolicy -Scope CurrentUser Unrestricted'
  ]);
  child = spawn('powershell.exe', [scriptPath]);
  child.stdout.on('data', function(data) {
    const cleanedData = data.toString().trim();
    if (cleanedData) {
      output.push(cleanedData);
    }
  });
  child.stderr.on('data', function(data) {
    console.log(`Powershell Errors: ${data}`);
  });
  child.on('exit', function() {
    console.log('Powershell Script finished');
    let verified = false;
    const username = output[1];
    const password = output[2];
    if (output && output[0] === 'True') {
      verified = true;
    }
    return { verified, username, password };
  });
  child.stdin.end(); // end input
}

module.exports = inputCredentials;
