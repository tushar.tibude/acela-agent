const { spawn } = require('child_process');

let child;
const scriptPath = require('path').join(
  process.cwd(),
  './check-service-logon-permission.ps1'
);

// eslint-disable-next-line import/prefer-default-export
function checkAccountLogonAsServicePermission(account) {
  spawn('powershell.exe', [
    'Set-ExecutionPolicy -Scope CurrentUser Unrestricted'
  ]);
  // eslint-disable-next-line prefer-const
  child = spawn('powershell.exe', [scriptPath, account]);
  const output = [];
  child.stdout.on('data', function(data) {
    const cleanedData = data.toString().trim();
    if (cleanedData) {
      output.push(cleanedData);
    }
  });
  child.stderr.on('data', function(data) {
    console.log(`Powershell Errors: ${data}`);
  });
  child.on('exit', function() {
    console.log('Powershell Script finished');
    let havePermission = false;
    if (output[0] === 'True') {
      havePermission = true;
    }
    return havePermission;
  });
  child.stdin.end(); // end input
}

module.exports = checkAccountLogonAsServicePermission;
