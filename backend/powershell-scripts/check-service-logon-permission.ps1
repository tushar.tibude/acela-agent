Import-Module .\UserRights.psm1

$accountWithServiceRights = Get-AccountsWithUserRight -Right SeServiceLogonRight
$userNames = @()
foreach ($account in $accountWithServiceRights)
{
  $userNames = $userNames + $account.Split("\")[-1]
}
$userNames -contains $args

