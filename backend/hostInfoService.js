const axios = require('axios');
const os = require('os');

const { EventLogger } = require('node-windows');
const BaseService = require('./baseService');

const log = new EventLogger('Acela Collector Service');
const POLLING_INTERVAL_IN_SEC = 5 * 60;
const SERVICE_NAME = 'HostInfoService';

class HostInfoService extends BaseService {
  constructor() {
    super(SERVICE_NAME, POLLING_INTERVAL_IN_SEC);
  }

  sendHostInfo = hostInfo => {
    const payload = {
      token: this.getCollectorToken(),
      service_data: {
        service_name: this.name,
        data: hostInfo
      }
    };
    try {
      return axios.post(this.urls.DATA_POST_URL, payload);
    } catch (error) {
      log.warn(error);
    }
  };

  getIPAddress = () => {
    const ifaces = os.networkInterfaces();
    let address;

    Object.keys(ifaces).forEach(dev => {
      // eslint-disable-next-line array-callback-return
      ifaces[dev].filter(details => {
        if (details.family === 'IPv4' && details.internal === false) {
          address = details.address;
        }
      });
    });
    return address;
  };

  getHostInfo = () => {
    const hostname = os.hostname();
    const architecture = os.arch();
    const ipAddress = this.getIPAddress();
    const platform = os.platform();
    const osType = os.type();
    const uptime = os.uptime();
    return {
      hostname,
      architecture,
      ipAddress,
      platform,
      osType,
      uptime
    };
  };

  checkPreCondition = () => {
    return this.storage.tokenExists();
  };

  process = () => {
    const hostInfo = this.getHostInfo();
    this.sendHostInfo(hostInfo);
  };
}

module.exports = HostInfoService;
