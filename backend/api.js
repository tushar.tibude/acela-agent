const http = require('http');
const querystring = require('querystring');

// eslint-disable-next-line import/prefer-default-export
export function validateToken(token) {
  const postData = querystring.stringify({
    token
  });
  const options = {
    host: 'dev.acela.io',
    port: 8000,
    path: '/api/v1/collector-tokens',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };

  const req = http.request(options, res => {
    console.log(`STATUS: ${res.statusCode}`);
    console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
    res.setEncoding('utf8');
    res.on('data', chunk => {
      console.log(`BODY: ${chunk}`);
    });
    res.on('end', () => {
      console.log('No more data in response.');
    });
  });

  req.on('error', e => {
    console.error(`problem with request: ${e.message}`);
  });

  // Write data to request body
  req.write(postData);
  req.end();
}
